<div align="center">
  
# Calculator
  
</div>

JavaScript Simplest Calculator with Alert  

Web Development AWARI FullStack Course
 
<div align="right">
   
## Deploy at: <a href="https://github.com/malanski/Calculator/">Calculator</a>  
   
</div>

## Objectives:  
- Create a Online Calculator SASS + HTML
- Use JavaScript 
- Customizate Design
  
This project is one of my first ever applications creating a responsive Calculator using HTML CSS and JavaScript.   
 
  
### Technologies:
- HTML5
- CSS3
- JavaScript
- FontAwesome
- Git
- Github  


<div align="left" margin-top="-150px">
    
| <img height="100px" src="https://avatars.githubusercontent.com/u/87362996?v=4"> | <a href="https://github.com/malanski">Ulisses Malanski</a> " - Web Developer/Visual Artist and musician in his spare time.  |
| ----------- | ----------- |
|  |  |



